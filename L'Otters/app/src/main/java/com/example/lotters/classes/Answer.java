package com.example.lotters.classes;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class Answer {


    public Answer(){
        //empty constructor
    }

    public Answer(String title, String answerBody, String preview, String username, String userID , String answerUID, String fishesCost, String userRank,
                  String questionID, String date, String previewCost, String answerBodyBoughtBy, String previewBoughtBy, int budget, Uri picUri){
        this.title = title;
        this.answerBody = answerBody;
        this.preview = preview;
        this.username = username;
        this.userID = userID;
        this.answerUID = answerUID;
        this.fishesCost = fishesCost;
        this.userRank = userRank;
        this.questionID = questionID;
        this.date = date;
        this.previewCost = previewCost;
        this.answerBodyBoughtBy = answerBodyBoughtBy;
        this.previewBoughtBy = previewBoughtBy;
        this.budget = budget;
        this.picUri = picUri;
    }

    private String title;
    private String answerBody;
    private String preview;
    private String username;
    private String userID;
    private String answerUID;
    private String fishesCost;
    private String previewCost;
    private String userRank;
    private String questionID;
    private String date;
    private String answerBodyBoughtBy;
    private String previewBoughtBy;
    private Integer budget;
    private Uri picUri;


    public Integer getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String getAnswerBodyBoughtBy() {
        return answerBodyBoughtBy;
    }

    public void setAnswerBodyBoughtBy(String answerBodyBoughtBy) {
        this.answerBodyBoughtBy = answerBodyBoughtBy;
    }

    public String getPreviewBoughtBy() {
        return previewBoughtBy;
    }

    public void setPreviewBoughtBy(String previewBoughtBy) {
        this.previewBoughtBy = previewBoughtBy;
    }

    public String getPreviewCost() {
        return previewCost;
    }

    public void setPreviewCost(String previewCost) {
        this.previewCost = previewCost;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    public String getUserRank() {
        return userRank;
    }

    public void setUserRank(String userRank) {
        this.userRank = userRank;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFishesCost() {
        return fishesCost;
    }

    public void setFishesCost(String fishesCost) {
        this.fishesCost = fishesCost;
    }

    public String getAnswerBody() {
        return answerBody;
    }

    public void setAnswerBody(String answerBody) {
        this.answerBody = answerBody;
    }

    public String getAnswerUID() {
        return answerUID;
    }

    public void setAnswerUID(String answerUID) {
        this.answerUID = answerUID;
    }
}
