package com.example.lotters.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lotters.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class loginActivity extends AppCompatActivity {

    private Button loginButton, registerButton;
    private EditText loginText, passwordText;
    private FirebaseUser currentUser;
    private FirebaseAuth mAuth;
    private Boolean pressAgain = false;
    private FirebaseFirestore database = FirebaseFirestore.getInstance();

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @Override
    public void onBackPressed() {
        if (pressAgain) {
            this.finishAffinity();
        } else {
            Toast.makeText(this, "Press back again to exit.",
                    Toast.LENGTH_SHORT).show();
            pressAgain = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pressAgain = false;
                }
            }, 3 * 1000);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginButton = findViewById(R.id.loginButton);
        registerButton = findViewById(R.id.registerButton);
        loginText = findViewById(R.id.username);
        passwordText = findViewById(R.id.password);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getApplicationContext(), registerActivity.class);
                startActivity( intent );
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isNetworkAvailable())
                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();

                else {

                    if (currentUser == null) {
                        String email;
                        String password;

                        email = loginText.getText().toString();
                        password = passwordText.getText().toString();

                        if (email.matches("") || password.matches(""))
                            Toast.makeText(getApplicationContext(), "Enter both E-mail and Password", Toast.LENGTH_SHORT).show();

                        else {
                            mAuth.signInWithEmailAndPassword(email, password)
                                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Incorrect login credentials", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }
                    } else {
                        finish();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);

                    }
                }
            }
        });


    }

}