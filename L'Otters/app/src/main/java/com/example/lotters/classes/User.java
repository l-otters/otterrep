package com.example.lotters.classes;

public class User {

    public User(){
        //empty constructor
    }

    public User( String email, String username, String birthdate, String fishes, String crabs, String onlineStatus, String rank, Integer xp ){

        //parameter constructor

        this.email = email;
        this.username = username;
        this.birthdate = birthdate;
        this.fishes = fishes;
        this.crabs = crabs;
        this.onlineStatus = onlineStatus;
        this.rank = rank;
        this.xp = xp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getFishes() {
        return fishes;
    }

    public void setFishes(String fishes) {
        this.fishes = fishes;
    }

    public String getCrabs() {
        return crabs;
    }

    public void setCrabs(String crabs) {
        this.crabs = crabs;
    }

    public String getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(String onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Integer getXp() {
        return xp;
    }

    public void setXp(Integer xp) {
        this.xp = xp;
    }

    private String email;
    private String username;
    private String birthdate;
    private String fishes;
    private String crabs;
    private String onlineStatus;
    private String rank;
    private Integer xp;

}
