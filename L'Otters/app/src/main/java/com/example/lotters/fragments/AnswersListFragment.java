package com.example.lotters.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.example.lotters.R;
import com.example.lotters.ViewModels.QuestionViewmodel;
import com.example.lotters.classes.Answer;
import com.example.lotters.classes.Question;
import com.example.lotters.recyclerviews.AnswersRecyclerView;
import com.example.lotters.recyclerviews.QuestionsRecyclerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.shape.InterpolateOnScrollPositionChangeHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AnswersListFragment extends Fragment implements AnswersRecyclerView.answerCardListener{

    private Button addAnswerButton;
    private ImageButton backButton;
    private AnswersRecyclerView adapter;
    private ArrayList<Answer> answerCards = new ArrayList<>();
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser currentUser = firebaseAuth.getCurrentUser();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference dbAnswers = db.collection("Answers");
    private ListenerRegistration listenerRegistration;
    private String questionUID;
    private QuestionViewmodel viewModel;
    private int posYsaved;

    public AnswersListFragment() {
        // Required empty public constructor
    }


    public static AnswersListFragment newInstance(String questionUID, int posYsaved) {
        AnswersListFragment fragment = new AnswersListFragment();
        Bundle args = new Bundle();
        args.putString("questionUID", questionUID);
        args.putInt("posYsaved", posYsaved);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            questionUID = getArguments().getString("questionUID");
            posYsaved = getArguments().getInt("posYsaved");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_answers_list, container, false);

        addAnswerButton = view.findViewById(R.id.alfAddAnswerButton);
        backButton = view.findViewById(R.id.alfBackButton);

        viewModel = new ViewModelProvider(getActivity()).get(QuestionViewmodel.class);

        addAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arguments = new Bundle();
                arguments.putString("questionUID", questionUID);
                Navigation.findNavController(view).navigate(R.id.navigateFromListFragmentToAnswerFormular, arguments);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arguments = new Bundle();
                arguments.putInt("posYtoScrollTo", posYsaved);
                Navigation.findNavController(view).navigate(R.id.navigateFromListFragmentToAnswersFragment, arguments);
            }
        });

        RecyclerView recyclerView = view.findViewById(R.id.alfAnswersRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new AnswersRecyclerView(getContext(), answerCards, this, false, currentUser.getUid());
        recyclerView.setAdapter(adapter);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (currentUser != null && questionUID != null) {

            listenerRegistration = dbAnswers.whereEqualTo("questionID", questionUID).orderBy("date", Query.Direction.DESCENDING).limit(10)
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                            if (error != null) {
                                Log.d("alf observer: ", "error");
                                return;
                            }

                            List<Answer> databaseAnswers = new ArrayList<>();

                            for (QueryDocumentSnapshot documentSnapshot : value) {
                                if (documentSnapshot.exists())
                                    databaseAnswers.add(documentSnapshot.toObject(Answer.class));
                            }

                            updateAnswerCards(databaseAnswers);

                        }
                    });

        }

    }

    @Override
    public void onStart() {
        super.onStart();

        initializeAnswerCards(questionUID);
    }

    private void initializeAnswerCards(String questionUID) {

        if ( currentUser != null && questionUID != null ) {

            dbAnswers.whereEqualTo("questionID", questionUID).orderBy("date", Query.Direction.DESCENDING).limit(10).get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {
                            List<Answer> databaseAnswers = new ArrayList<>();

                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()){
                                if ( documentSnapshot.exists()) databaseAnswers.add(documentSnapshot.toObject(Answer.class));
                            }

                            updateAnswerCards(databaseAnswers);
                        }
                    });
        }
    }


    private void updateAnswerCards(List<Answer> databaseAnswers) {

        answerCards.clear();
        answerCards.addAll(databaseAnswers);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onBuyBodyButtonPressed(int index) {

    }

    @Override
    public void onBuyPreviewButtonPressed(int index) {

    }

    @Override
    public void onRemoveButtonPressed(int index) {

        String toDelete = answerCards.get(index).getAnswerUID();

        dbAnswers.document(toDelete).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if ( task.isSuccessful() ){
                    Log.d("question delete", "success");
                } else {
                    Log.d("question delete", "failuire");
                }
            }
        });
    }
}