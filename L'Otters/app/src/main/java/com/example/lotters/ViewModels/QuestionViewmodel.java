package com.example.lotters.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.lotters.classes.Question;

public class QuestionViewmodel extends ViewModel {

    public LiveData<String> getQuestionUID() {
        return questionUID;
    }

    public void setQuestionUID(String questionUID) {
        this.questionUID.setValue(questionUID);
    }

    public LiveData<String> getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery.setValue(searchQuery);
    }

    public LiveData<Boolean> getPerformSearch() {
        return performSearch;
    }

    public void setPerformSearch(Boolean performSearch) {
        this.performSearch.setValue(performSearch);
    }

    private MutableLiveData<String> questionUID = new MutableLiveData<>();
    private MutableLiveData<String> searchQuery = new MutableLiveData<>();
    private MutableLiveData<Boolean> performSearch = new MutableLiveData<>();
}
