package com.example.lotters.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.lotters.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;


public class ChangePasswordFragment extends Fragment {

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser currentUser = firebaseAuth.getCurrentUser();
    private String email,password;
    private EditText currentPassword, newPassword, confNewPassword;
    private Button confirmChangePass;
    private ImageView backToSettings;


    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        currentPassword = view.findViewById(R.id.current_pass);
        newPassword = view.findViewById(R.id.new_pass);
        confNewPassword = view.findViewById(R.id.conf_new_pass);
        confirmChangePass = view.findViewById(R.id.confirm_btn_pass);
        backToSettings = view.findViewById(R.id.cpf_backbutton);

        confirmChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(currentPassword.getText().toString().matches("") || newPassword.getText().toString().matches("")
                || confNewPassword.getText().toString().matches(""))
                    Toast.makeText(getContext(), "All fields are mandatory", Toast.LENGTH_SHORT).show();

                else {
                    email = currentUser.getEmail();
                    password = currentPassword.getText().toString();
                    AuthCredential authCredential = EmailAuthProvider.getCredential(email, password);
                    currentUser.reauthenticate(authCredential).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            if (newPassword.getText().toString().equals(confNewPassword.getText().toString())) {

                                currentUser.updatePassword(newPassword.getText().toString());
                                Navigation.findNavController(view).navigate(R.id.navigateFromChangePasswordFragmentToSettingsFragment);
                            } else {
                                Toast.makeText(getContext(), "Passwords don't match", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull @NotNull Exception e) {
                            Toast.makeText(getContext(), "Wrong current password", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        });

        backToSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateFromChangePasswordFragmentToSettingsFragment);
            }
        });

        return view;
    }
}