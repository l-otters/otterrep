package com.example.lotters.recyclerviews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lotters.R;

import java.util.List;

public class PreferencesRecyclerView extends RecyclerView.Adapter<PreferencesRecyclerView.PreferenceViewHolder> {

    private List<String> data;
    private LayoutInflater layoutInflater;
    private OnRemoveListener listener;

    public PreferencesRecyclerView(Context context, List<String> fetchedData, OnRemoveListener listener){
        this.layoutInflater = LayoutInflater.from(context);
        this.data = fetchedData;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PreferenceViewHolder onCreateViewHolder (@NonNull ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.preference_layout, parent, false);
        return new PreferenceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PreferencesRecyclerView.PreferenceViewHolder holder, int position) {

        holder.preferenceText.setText(data.get(position));
        holder.preferenceText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRemove(position);
                data.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, data.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (data != null ) return data.size();
        else return 0;
    }

    public class PreferenceViewHolder extends RecyclerView.ViewHolder {

        TextView preferenceText;

        PreferenceViewHolder ( View itemView ){
            super(itemView);
            preferenceText = itemView.findViewById(R.id.pLPreferenceText);

        }

    }

    public interface OnRemoveListener {
        void onRemove(int index);
    }
}
