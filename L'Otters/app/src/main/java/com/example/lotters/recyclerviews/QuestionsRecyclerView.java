package com.example.lotters.recyclerviews;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lotters.R;
import com.example.lotters.classes.Answer;
import com.example.lotters.classes.Question;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class QuestionsRecyclerView extends RecyclerView.Adapter<QuestionsRecyclerView.ViewHolder> implements Filterable {

    private List<Question> data;
    private LayoutInflater layoutInflater;
    private AdapterView.OnItemClickListener itemClickListener;
    private onRemoveQuestionCardListener listener;
    private Boolean visibility;
    private List<Question> dataFull;
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    private StorageReference picRef;

    public QuestionsRecyclerView(Context context, List<Question> fetchedData, onRemoveQuestionCardListener listener, Boolean visibility){
        this.layoutInflater = LayoutInflater.from(context);
        this.data = fetchedData;
        dataFull = new ArrayList<>(data);
        this.listener = listener;
        this.visibility = visibility;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.qrow_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String title, description, username, age, gender, status;
        List<String> tagList = new ArrayList<>();
        Integer budget;

        picRef = storageReference.child("Users/"+data.get(position).getUserID()+"/profile.jpg");
        if ( picRef != null )
        picRef.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<Uri> task) {
                if ( task.isSuccessful()){
                    Picasso.get().load(task.getResult()).into(holder.profilePic);
                }
            }
        });

        status = data.get(position).getStatus();
        holder.status.setText(status);

        title = data.get(position).getTitle();
        holder.title.setText(title);

        description = data.get(position).getDescription();
        holder.description.setText(description);

        username = data.get(position).getUsername();
        holder.username.setText(username);

        gender = data.get(position).getGender();
        holder.gender.setText(gender);

        budget = data.get(position).getBudget();
        holder.budget.setText(String.valueOf(budget));

        age = data.get(position).getAge();
        holder.age.setText(age);

        for ( String tag : data.get(position).getTags())
            tagList.add(tag);

        holder.adapter = new TagsRecyclerView(holder.status.getContext(), tagList);
        holder.recyclerView.setAdapter(holder.adapter);

        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRemoveQuestion(position);
                data.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, data.size());
            }
        });

        holder.answers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onAnswerButtonPressed(position);
            }
        });

        if ( !visibility ) holder.removeButton.setVisibility(View.INVISIBLE);

        holder.showMoreShowLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMaxLines(holder.description, holder.showMoreShowLess, 3);
            }
        });

        if ( holder.description.getLineCount() >= 3 ){
            holder.showMoreShowLess.setVisibility(View.VISIBLE);
            holder.description.setMaxLines(3);
        } else {
            holder.showMoreShowLess.setVisibility(View.INVISIBLE);
            holder.description.setMaxLines(Integer.MAX_VALUE);
        }

    }

    private void setMaxLines( TextView textView,TextView showMoreLess, int maxLines ){
        if (textView.getMaxLines() <= maxLines) {
            textView.setMaxLines(Integer.MAX_VALUE);
            showMoreLess.setText("show less");
        }
        else {
            textView.setMaxLines(maxLines);
            showMoreLess.setText("show more");
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, description, username, gender, status, age, showMoreShowLess, budget;
        Button removeButton, answers;
        RecyclerView recyclerView;
        TagsRecyclerView adapter;
        ImageView profilePic;

        ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.qTitle);
            removeButton = itemView.findViewById(R.id.qRemoveButton);
            description = itemView.findViewById(R.id.qDescriptionText);
            username = itemView.findViewById(R.id.qUserEmail);
            gender = itemView.findViewById(R.id.qGender);
            status = itemView.findViewById(R.id.qStatus);
            answers = itemView.findViewById(R.id.qAnswersButton);
            age = itemView.findViewById(R.id.qAge);
            recyclerView = itemView.findViewById(R.id.qtagsRecyclerView);
            showMoreShowLess = itemView.findViewById(R.id.qShowMoreShowLessDescr);
            showMoreShowLess.setVisibility(View.INVISIBLE);
            budget = itemView.findViewById(R.id.qBudget);
            profilePic = itemView.findViewById(R.id.qProfilePicture);

        }
    }

    public interface onRemoveQuestionCardListener {
        void onRemoveQuestion(int index);
        void onAnswerButtonPressed(int index);
    }

    @Override
    public Filter getFilter() {
        return dataFilter;
    }

    private Filter dataFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Question> filteredList = new ArrayList<>();

            if ( constraint == null || constraint.length() == 0 ){
                filteredList.addAll(dataFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for ( Question question : dataFull ){
                    List<String> tags = question.getTags();
                    for (String tag : tags){
                        String nonSensitiveTag = tag.toLowerCase();
                        if ( nonSensitiveTag.contains(filterPattern) ){
                            filteredList.add(question);
                        }
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data.clear();
            data.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

}
