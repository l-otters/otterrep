package com.example.lotters.classes;

import android.net.Uri;
import android.os.Bundle;

import com.google.firebase.storage.StorageReference;

import java.util.List;

public class Question {


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }


    public Question(String description, String gender, String status, String age, String title, String userID, String username,
                    String date, String questionID, List<String> tags, int budget) {
        this.description = description;
        this.gender = gender;
        this.status = status;
        this.age = age;
        this.title = title;
        this.tags = tags;
        this.userID = userID;
        this.username = username;
        this.date = date;
        this.questionID = questionID;
        this.budget = budget;
    }



    private String description;
    private String questionID;
    private String date;
    private String gender;
    private String status;
    private String age;
    private String title;
    private String userID;
    private String username;
    private List<String> tags;
    private int budget;

    public Question(){
        //empty constructor
    }

}
