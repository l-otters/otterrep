package com.example.lotters.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.lotters.R;
import com.example.lotters.ViewModels.QuestionViewmodel;
import com.example.lotters.classes.Question;
import com.example.lotters.recyclerviews.QuestionsRecyclerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class QuestionsFragment extends Fragment implements QuestionsRecyclerView.onRemoveQuestionCardListener {

    private QuestionsRecyclerView adapter;
    private Button addButton;
    private ArrayList<Question> questionCards = new ArrayList<>();
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser currentUser = firebaseAuth.getCurrentUser();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference dbUsers = db.collection("Users");
    private CollectionReference dbQuestions = db.collection("Questions");
    private CollectionReference dbAnswers = db.collection("Answers");
    private ListenerRegistration listenerRegistration;
    private int scrollPosition;
    private QuestionViewmodel viewmodel;
    private RecyclerView recyclerView;


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    public QuestionsFragment() {
        // Required empty public constructor
    }

    public static QuestionsFragment newInstance(int scrollPosition) {
        QuestionsFragment fragment = new QuestionsFragment();
        Bundle args = new Bundle();
        args.putInt("scrollPosition", scrollPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            scrollPosition = getArguments().getInt("scrollPosition");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_questions, container, false);

        addButton = view.findViewById(R.id.addButton);

        recyclerView = view.findViewById(R.id.qRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new QuestionsRecyclerView(getContext(), questionCards, this, true);
        recyclerView.setAdapter(adapter);

        viewmodel = new ViewModelProvider(getActivity()).get(QuestionViewmodel.class);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if ( !recyclerView.canScrollVertically(1)){
                    refreshQuestions();
                }
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateFromQuestionsToQuestionFormular);
            }
        });

        return view;
    }

    private void refreshQuestions() {

        int size = questionCards.size();

        if ( size-1 >= 0 ){
            String lastQuestionDate = questionCards.get(size-1).getDate();

            dbQuestions.whereLessThan("date", lastQuestionDate).whereEqualTo("userID", currentUser.getUid()).orderBy("date", Query.Direction.DESCENDING)
                    .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {
                    if ( task.isSuccessful() ){
                        List<Question> databaseQuestions = new ArrayList<>();

                        for ( QueryDocumentSnapshot doc : task.getResult()){
                            if (doc.exists()) {
                                databaseQuestions.add(doc.toObject(Question.class));
                            }
                        }

                        addToQuestionList(databaseQuestions);
                    }
                }
            });

        }

    }

    private void addToQuestionList(List<Question> list){
        questionCards.addAll(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (currentUser != null) {

            listenerRegistration = dbQuestions.whereEqualTo("userID", currentUser.getUid())
                    .orderBy("date", Query.Direction.DESCENDING).limit(10)
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                            if (error != null) {
                                Log.d("listener", "error");
                                return;
                            }

                            List<Question> databaseQuestions = new ArrayList<>();

                            for (QueryDocumentSnapshot document : value) {
                                if (document.exists()) {
                                    databaseQuestions.add(document.toObject(Question.class));
                                }
                            }

                            updateRecyclerView(databaseQuestions);
                        }
                    });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        listenerRegistration.remove();
    }

    private void updateRecyclerView(List<Question> databaseQuestions) {

        questionCards.clear();
        questionCards.addAll(databaseQuestions);
        adapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(scrollPosition);

    }

    @Override
    public void onRemoveQuestion(int index) {

        if (!isNetworkAvailable())
            Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();

        else {
            String toDelete = questionCards.get(index).getQuestionID();

            dbQuestions.document(toDelete).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Log.d("question delete", "success");
                    } else {
                        Log.d("question delete", "failure");
                    }
                }
            });
        }
    }

    @Override
    public void onAnswerButtonPressed(int index) {

        if (!isNetworkAvailable())
            Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();

        else {
            NavController navController;
            NavHostFragment navHostFragment;

            navHostFragment = (NavHostFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.navhost);
            navController = navHostFragment.getNavController();

            Bundle arguments = new Bundle();
            arguments.putString("questionUID", questionCards.get(index).getQuestionID());
            arguments.putInt("scrollPosition", index);

            navController.navigate(R.id.navigateFromQuestionsToAnswersForQuestion, arguments);
        }
    }
}