package com.example.lotters.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lotters.R;
import com.example.lotters.ViewModels.QuestionViewmodel;
import com.example.lotters.activities.loginActivity;
import com.example.lotters.classes.User;
import com.example.lotters.fragments.AnswersFragment;
import com.example.lotters.fragments.AnswersListFragment;
import com.example.lotters.fragments.QuestionsFragment;
import com.example.lotters.fragments.SettingsFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private Boolean pressAgain = false;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference dbUsers = db.collection("Users");
    private BottomNavigationView bottomNav;
    private NavController navController;
    private NavHostFragment navHostFragment;
    private TextView appText;
    private SearchView searchView;
    private QuestionViewmodel viewModel;
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    @Override
    public void onBackPressed() {
        if (pressAgain) {
            this.finishAffinity();
        } else {
            Toast.makeText(this, "Press back again to exit.",
                    Toast.LENGTH_SHORT).show();
            pressAgain = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pressAgain = false;
                }
            }, 3 * 1000);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNav = findViewById(R.id.mainBottomNavView);
        appText = findViewById(R.id.appText);
        searchView = findViewById(R.id.mainMenuSearchView);
        viewModel = new ViewModelProvider(this).get(QuestionViewmodel.class);

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( appText.getVisibility() == View.VISIBLE ) appText.setVisibility(View.INVISIBLE);
                navController.navigate(R.id.answersFragment);
                viewModel.setPerformSearch(true);
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                if ( appText.getVisibility() == View.INVISIBLE ) appText.setVisibility(View.VISIBLE);
                viewModel.setSearchQuery("");
                viewModel.setPerformSearch(false);

                return false;
            }
        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                viewModel.setSearchQuery(newText);

                return false;
            }
        });

        navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.navhost);
        navController = navHostFragment.getNavController();

        Bundle bundle = new Bundle();

        NavigationUI.setupWithNavController(bottomNav, navController);

        mAuth = FirebaseAuth.getInstance();

        currentUser = mAuth.getCurrentUser();

    }


    @Override
    public void onStart() {
        super.onStart();
        if ( currentUser == null ) {
           Intent intent = new Intent(getApplicationContext(), loginActivity.class);
           startActivity(intent);
        }
    }
}