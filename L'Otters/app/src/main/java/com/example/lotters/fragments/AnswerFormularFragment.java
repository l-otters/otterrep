package com.example.lotters.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;

import com.example.lotters.R;
import com.example.lotters.ViewModels.QuestionViewmodel;
import com.example.lotters.classes.Answer;
import com.example.lotters.classes.Question;
import com.example.lotters.classes.User;
import com.example.lotters.recyclerviews.QuestionsRecyclerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.slider.Slider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AnswerFormularFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AnswerFormularFragment extends Fragment {

    private String questionUID;
    private QuestionViewmodel viewmodel;
    private EditText title, preview, answerBody, fishes, previewCost;
    private Slider budgetSlider;
    private int budget = 0;

    private ImageButton backButton;
    private Button send;

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseUser currentUser = firebaseAuth.getCurrentUser();
    private CollectionReference dbAnswers = db.collection("Answers");

    public AnswerFormularFragment() {
        // Required empty public constructor
    }

    public static AnswerFormularFragment newInstance(String questionUID) {
        AnswerFormularFragment fragment = new AnswerFormularFragment();
        Bundle args = new Bundle();
        args.putString("questionUID", questionUID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            questionUID = getArguments().getString("questionUID");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_answer_formular, container, false);

        backButton = view.findViewById(R.id.afReturntoAnswers);
        title = view.findViewById(R.id.afTitle);
        preview = view.findViewById(R.id.afPreview);
        answerBody = view.findViewById(R.id.afQuestionBody);
        fishes = view.findViewById(R.id.afFishesCost);
        previewCost = view.findViewById(R.id.afPreviewCost);
        send = view.findViewById(R.id.afPostQuestion);
        budgetSlider = view.findViewById(R.id.afBudgetSlider);

        budgetSlider.setValueFrom(0);
        budgetSlider.setValueTo(500);
        budgetSlider.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull @NotNull Slider slider, float value, boolean fromUser) {
                budget = Math.round(value);
            }
        });

        viewmodel =  new ViewModelProvider(getActivity()).get(QuestionViewmodel.class);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arguments = new Bundle();
                arguments.putString("questionUID", questionUID);
                Navigation.findNavController(view).navigate(R.id.navigateFromAnswerFormularToListFragment, arguments);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postToDatabase();
                Bundle arguments = new Bundle();
                arguments.putString("questionUID", questionUID);
                Navigation.findNavController(view).navigate(R.id.navigateFromAnswerFormularToListFragment, arguments);
            }
        });

        return view;
    }

    private void postToDatabase(){

        Answer answer = new Answer();
        String answerID = UUID.randomUUID().toString();

        answer.setBudget(budget);

        if ( !title.getText().toString().equals("") && !fishes.getText().toString().equals("")
                && !preview.getText().toString().equals("") && !answerBody.getText().toString().equals("")
                && !previewCost.getText().toString().equals("")) {

            answer.setTitle(title.getText().toString());
            answer.setAnswerUID(answerID);
            answer.setFishesCost(fishes.getText().toString());
            answer.setPreview(preview.getText().toString());
            answer.setAnswerBody(answerBody.getText().toString());
            answer.setPreviewCost(previewCost.getText().toString());
            answer.setAnswerBodyBoughtBy("none");
            answer.setPreviewBoughtBy("none");

        } else {

            answer.setTitle("none");
            answer.setAnswerUID(answerID);
            answer.setFishesCost("0");
            answer.setPreview("none");
            answer.setAnswerBody("none");
            answer.setPreviewCost("0");
            answer.setPreviewBoughtBy("none");
            answer.setAnswerBodyBoughtBy("none");

        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = new Date();
        String dateTime = dateFormat.format(date);

        answer.setDate(dateTime);

        if ( currentUser != null ){

            db.collection("Users").document(currentUser.getUid()).get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if ( task.isSuccessful() ){

                                User user = task.getResult().toObject(User.class);

                                answer.setUserRank(user.getRank());
                                answer.setUserID(currentUser.getUid());
                                answer.setUsername(user.getUsername());
                                answer.setQuestionID(questionUID);
                                dbAnswers.document(answerID).set(answer);

                            } else {
                                Log.d("add answer to database", "failuire");
                            }
                        }
                    });
        }

        db.collection("Users").document(currentUser.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<DocumentSnapshot> task) {
                if ( task.isSuccessful() ){
                    User user = task.getResult().toObject(User.class);

                    db.collection("Questions").document(questionUID).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull @NotNull Task<DocumentSnapshot> task) {
                            if ( task.isSuccessful() ){
                                Question question = task.getResult().toObject(Question.class);
                                if ( !question.getUserID().equals(currentUser.getUid()) )
                                    db.collection("Users").document(currentUser.getUid()).update("xp",  user.getXp() + 5);
                            }
                        }
                    });

                }
            }
        });

    }

}