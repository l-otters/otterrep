package com.example.lotters.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lotters.R;
import com.example.lotters.activities.MainActivity;
import com.example.lotters.activities.loginActivity;
import com.example.lotters.activities.registerActivity;
import com.example.lotters.classes.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.transition.platform.MaterialSharedAxis;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;


import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

public class MainFragment extends Fragment {

    private Button logoutButton;
    private ImageButton usernameEdit, birthdateEdit, usernameDone, birthdateDone,profilePicEdit, profilePicReset;
    private TextView username, birthdate, fishes, birthdateSelect, xpText, rank;
    private EditText usernameEditText;
    private DatePickerDialog.OnDateSetListener editDateSetListener;
    private ImageView profilePic;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference dbUsers = db.collection("Users");
    private StorageReference storageReference;



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    private ProgressBar progressBar;
    //private Integer progressMaxValue = 100;


    public MainFragment() {
        // Required empty public constructor
    }


    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*if (getArguments() != null) {
        }*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        logoutButton = view.findViewById(R.id.mfLogoutButton);
        usernameEdit = view.findViewById(R.id.mfUsernameEditBtn);
        birthdateEdit = view.findViewById(R.id.mfBirthdateEditBtn);
        usernameDone = view.findViewById(R.id.mfUsernameDone);
        birthdateDone = view.findViewById(R.id.mfBirthdateDone);
        username = view.findViewById(R.id.mfUsername);
        usernameEditText = view.findViewById(R.id.mfUsernameEdit);
        birthdate = view.findViewById(R.id.mfBirthdate);
        birthdateSelect = view.findViewById(R.id.mfBirthdateEdit);
        fishes = view.findViewById(R.id.mfFishes);
        progressBar = view.findViewById(R.id.mfProgressBar);
        xpText = view.findViewById(R.id.mfXp);
        rank = view.findViewById(R.id.mfRank);
        profilePic = view.findViewById(R.id.mfProfilePicture);
        profilePicEdit = view.findViewById(R.id.mfEditProfilePicture);
        profilePicReset = view.findViewById(R.id.mfRemoveProfilePic);

        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        storageReference = FirebaseStorage.getInstance().getReference();



        updateUI();

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth.signOut();
                returnToLogin();
            }
        });

        profilePicEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openGalleryIntent = new Intent (Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                if(!isNetworkAvailable()) Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                else
                startActivityForResult(openGalleryIntent,1000);
            }
        });

        profilePicReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("android.resource://com.example.lotters/drawable/default_profile_pic");

                if(!isNetworkAvailable()) Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                else
                uploadImageToFirebase(uri);
            }
        });

        usernameEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isNetworkAvailable())
                    Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();

                else {

                    if (username.getVisibility() == View.VISIBLE) {

                        username.setVisibility(View.INVISIBLE);
                        usernameEditText.setVisibility(View.VISIBLE);
                        usernameDone.setVisibility(View.VISIBLE);

                    } else {

                        username.setVisibility(View.VISIBLE);
                        usernameEditText.setVisibility(View.INVISIBLE);
                        usernameDone.setVisibility(View.INVISIBLE);

                    }
                }
            }
        });

        usernameDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fetchedUsername = usernameEditText.getText().toString();

                if(!fetchedUsername.equals("")){

                    dbUsers.document(currentUser.getUid()).update("username",fetchedUsername);
                    usernameEditText.setText("");
                    username.setVisibility(View.VISIBLE);
                    usernameEditText.setVisibility(View.INVISIBLE);
                    usernameDone.setVisibility(View.INVISIBLE);

                    db.collection("Questions").whereEqualTo("userID", currentUser.getUid())
                            .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()){
                                for (QueryDocumentSnapshot documentSnapshot : task.getResult()){
                                    db.collection("Questions").document(documentSnapshot.getId()).update("username", fetchedUsername);
                                }
                            }
                        }
                    });

                    db.collection("Answers").whereEqualTo("userID", currentUser.getUid())
                            .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if ( task.isSuccessful() ) {
                                if ( task.getResult() != null )
                                    for ( QueryDocumentSnapshot documentSnapshot : task.getResult()){
                                        db.collection("Answers").document(documentSnapshot.getId()).update("username", fetchedUsername);
                                    }
                            }
                        }
                    });

                } else {
                    Toast.makeText(getContext(), "Please enter your username", Toast.LENGTH_SHORT).show();
                }

            }
        });

        birthdateEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isNetworkAvailable()) Toast.makeText(getContext(),"No internet connection", Toast.LENGTH_SHORT).show();

                else {

                    if (birthdate.getVisibility() == View.VISIBLE) {

                        birthdate.setVisibility(View.INVISIBLE);
                        birthdateSelect.setVisibility(View.VISIBLE);
                        birthdateDone.setVisibility(View.VISIBLE);
                        Toast.makeText(getContext(), "Tap the text to change birthdate", Toast.LENGTH_SHORT).show();

                    } else {

                        birthdate.setVisibility(View.VISIBLE);
                        birthdateSelect.setVisibility(View.INVISIBLE);
                        birthdateDone.setVisibility(View.INVISIBLE);

                    }
                }
            }
        });

        birthdateSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getActivity(), android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        editDateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        editDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                Log.d("Main Fragment:","editDateSet: dd/mm/yyyy: " + dayOfMonth + "/" + month + "/" + year);
                month=month+1;
                String birthDate = dayOfMonth + "/" + month + "/" + year;
                birthdateSelect.setText(birthDate);
            }
        };


        birthdateDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fetchedBirthdate = birthdateSelect.getText().toString();

                if(!fetchedBirthdate.equals("")){

                    dbUsers.document(currentUser.getUid()).update("birthdate",fetchedBirthdate);
                    birthdateSelect.setText("");
                    birthdate.setVisibility(View.VISIBLE);
                    birthdateSelect.setVisibility(View.INVISIBLE);
                    birthdateDone.setVisibility(View.INVISIBLE);

                } else {
                    Toast.makeText(getActivity(), "Choose a valid birthdate", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (currentUser != null) {
            birthdateSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dialog = new DatePickerDialog(
                            getActivity(), android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                            editDateSetListener, year, month, day);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();

                }
            });


            StorageReference profileRef = storageReference.child("Users/"+currentUser.getUid()+"/profile.jpg");

            profileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).into(profilePic);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Uri uri = Uri.parse("android.resource://com.example.lotters/drawable/default_profile_pic");
                    uploadImageToFirebase(uri);
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if ( currentUser != null ){
            dbUsers.document(currentUser.getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                    if (error != null) {
                        Log.d("Main Fragment: ", "Error Snapshot listener");

                        return;
                    }

                    User dbUser = new User();
                    dbUser = value.toObject(User.class);
                    updateUsernameBirthdate(dbUser.getUsername(), dbUser.getBirthdate());
                    updateFishesAndXp(dbUser.getFishes(), dbUser.getXp().toString());
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000){
            if (resultCode == Activity.RESULT_OK){
                Uri imageUri = data.getData();

                uploadImageToFirebase(imageUri);
            }
        }

    }

    private void uploadImageToFirebase(Uri imageUri) {
            StorageReference fileRef = storageReference.child("Users/"+currentUser.getUid()+"/profile.jpg");
                    fileRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getContext(),"Profile Picture Changed",Toast.LENGTH_SHORT).show();
                            fileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Picasso.get().load(uri).into(profilePic);
                                }
                            });
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull @NotNull Exception e) {
                            Toast.makeText(getContext(),"Profile Picture Change Failed",Toast.LENGTH_SHORT).show();
                        }
                    });
    }

    private void updateFishesAndXp(String fetchedFishes, String fetchedXP) {

        fishes.setText(fetchedFishes);
        xpText.setText(fetchedXP);

        if ( currentUser != null ){
            dbUsers.document(currentUser.getUid()).get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if ( task.isSuccessful() ){

                                User databaseUser = new User();
                                databaseUser = task.getResult().toObject(User.class);

                                if ( databaseUser.getXp() > getMaxProgress(databaseUser.getRank())) {
                                    dbUsers.document(currentUser.getUid()).update("rank", getRankByXp(databaseUser.getXp()));
                                    rank.setText(getRankByXp(databaseUser.getXp()));
                                    progressBar.setMax(getMaxProgress(getRankByXp(databaseUser.getXp())));
                                }
                                else{
                                    progressBar.setMax(getMaxProgress(databaseUser.getRank()));
                                    rank.setText(databaseUser.getRank());
                                }

                                progressBar.setProgress(Integer.parseInt(fetchedXP));


                            } else {
                                Log.d("updateFishesAndXp", "failure");
                            }
                        }
                    });
        }

        progressBar.setProgress(Integer.parseInt(xpText.getText().toString()));

    }

    private void updateUsernameBirthdate(String fetchedUsername, String fetchedBirthdate){

        username.setText(fetchedUsername);
        birthdate.setText(fetchedBirthdate);

    }

    private void updateUI(){
        if ( currentUser != null ){
            dbUsers.document(currentUser.getUid()).get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if ( task.isSuccessful() ){

                                User databaseUser = new User();
                                databaseUser = task.getResult().toObject(User.class);

                                username.setText(databaseUser.getUsername());
                                birthdate.setText(databaseUser.getBirthdate());
                                fishes.setText(databaseUser.getFishes());

                                Log.d("max progres::", String.valueOf(getMaxProgress(databaseUser.getRank())));

                                if ( databaseUser.getXp() > getMaxProgress(databaseUser.getRank())) {
                                    dbUsers.document(currentUser.getUid()).update("rank", getRankByXp(databaseUser.getXp()));
                                    rank.setText(getRankByXp(databaseUser.getXp()));
                                    progressBar.setMax(getMaxProgress(getRankByXp(databaseUser.getXp())));
                                }
                                else{
                                    progressBar.setMax(getMaxProgress(databaseUser.getRank()));
                                    rank.setText(databaseUser.getRank());
                                }

                                progressBar.setProgress(databaseUser.getXp());
                                xpText.setText(String.valueOf(databaseUser.getXp()));


                            } else {
                                Log.d("mainfragment", "user read failure");
                            }
                        }
                    });
        }
    }

    private int getMaxProgress(String rank){

        int maxProgress;

        switch (rank){
            case "Otter Knight":
                maxProgress = 150;
                break;
            case "Otter Veteran":
                maxProgress = 300;
                break;
            case "Folk Hero":
                maxProgress = 500;
                break;
            case "Mighty Beast":
                maxProgress = 750;
                break;
            case "Otter Legend":
                maxProgress = 1050;
                break;
            case "Unleashed Otter":
                maxProgress = 1400;
                break;
            case "Otter DemiGod":
                maxProgress = 1800;
                break;
            case "Otter God":
                maxProgress = 2250;
                break;
            case "Eternal":
                maxProgress = 2750;
                break;
            default:
                maxProgress = 50;
                break;
        }

        return maxProgress;
    }

    private String getRankByXp(int xp){

        String rank = "None";

        if ( xp < 50 ) rank = "Otter Squire";
        else if ( xp < 150 ) rank = "Otter Knight";
        else if ( xp < 300 ) rank = "Otter Veteran";
        else if ( xp < 500 ) rank = "Folk Hero";
        else if ( xp < 750 ) rank = "Mighty Beast";
        else if ( xp < 1050 ) rank = "Otter Legend";
        else if ( xp < 1400 ) rank = "Unleashed Otter";
        else if ( xp < 1800 ) rank = "Otter DemiGod";
        else if ( xp < 2250 ) rank = "Otter God";
        else if ( xp >= 2250 ) rank = "Eternal";

        return rank;
    }


    private void returnToLogin() {

        Intent intent = new Intent(getContext(), loginActivity.class);
        startActivity(intent);

    }

}