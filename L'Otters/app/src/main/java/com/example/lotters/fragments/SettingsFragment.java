package com.example.lotters.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lotters.R;
import com.example.lotters.classes.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;


public class SettingsFragment extends Fragment {

    private ImageButton changeEmail, changePassword, privacy;
    private TextView changeEmailText, changePasswordText, privacyText, username;
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference dbUsers = db.collection("Users");
    private FirebaseUser currentUser = firebaseAuth.getCurrentUser();
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    private StorageReference fileRef;
    private ImageView profilePic;

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    public SettingsFragment() {
        // Required empty public constructor
    }

    private void updateUI(){
        if ( currentUser != null ){
            dbUsers.document(currentUser.getUid()).get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if ( task.isSuccessful() ){

                                User databaseUser = new User();
                                databaseUser = task.getResult().toObject(User.class);

                                username.setText(databaseUser.getUsername());

                            } else {
                                Log.d("settingsFragment", "user read failure");
                            }
                        }
                    });
        }
    }


    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        changeEmail=view.findViewById(R.id.sfEmailNextBtn);
        changeEmailText=view.findViewById(R.id.sfEmail);
        changePassword=view.findViewById(R.id.sfPasswordNextBtn);
        changePasswordText=view.findViewById(R.id.sfPassword);
        privacy = view.findViewById(R.id.sfPrivacyNextBtn);
        privacyText = view.findViewById(R.id.sfPrivacy);
        username = view.findViewById(R.id.sfUsername);

        dbUsers.document(currentUser.getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if ( task.isSuccessful() ) {

                            User databaseUser = new User();
                            databaseUser = task.getResult().toObject(User.class);

                            username.setText(databaseUser.getUsername());
                        } else {
                            Log.d("SettingsFragment", "user read failure");
                        }
                    }
                });

        profilePic = view.findViewById(R.id.sfProfilePicture);

        if ( currentUser != null ){
             fileRef = storageReference.child("Users/"+currentUser.getUid()+"/profile.jpg");
             fileRef.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                 @Override
                 public void onComplete(@NonNull @NotNull Task<Uri> task) {
                    if ( task.isSuccessful()){
                        Picasso.get().load(task.getResult()).into(profilePic);
                    }
                 }
             });
        }


        changeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isNetworkAvailable()) Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                else
                Navigation.findNavController(view).navigate(R.id.navigateFromSettingsFragmentToChangeEmailFragment);
            }
        });

        changeEmailText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isNetworkAvailable()) Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                else
                Navigation.findNavController(view).navigate(R.id.navigateFromSettingsFragmentToChangeEmailFragment);
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isNetworkAvailable()) Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                else
                Navigation.findNavController(view).navigate(R.id.navigateFromSettingsFragmentToChangePasswordFragment);
            }
        });

        changePasswordText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isNetworkAvailable()) Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                else
                Navigation.findNavController(view).navigate(R.id.navigateFromSettingsFragmentToChangePasswordFragment);
            }
        });

        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Navigation.findNavController(view).navigate(R.id.navigateFromSettingsFragmentToPrivacyFragment);
            }
        });

        privacyText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Navigation.findNavController(view).navigate(R.id.navigateFromSettingsFragmentToPrivacyFragment);
            }
        });

        return view;
    }



    @Override
    public void onActivityCreated(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        updateUI();

    }

}